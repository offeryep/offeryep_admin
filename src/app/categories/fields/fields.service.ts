import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class fieldsService {
    public Url: string;
    public headers = new Headers({ 'Content-Type': 'application/json' });
    public options = new RequestOptions({
        headers: this.headers
    });
    constructor(public http: Http, public _config: Configuration) { }

    getFields(subCategoryName, opType) {
        let Url = this._config.Server + "fields/" + subCategoryName + "?type=" + opType;
        return this.http.get(Url, { headers: this._config.headers }).map(res => res.json());
    }
    addFields(fields, subCategoryName): Observable<any> {
        let url = this._config.Server + "fields/" + subCategoryName;
        let body = { filterId: fields.filterId, type: fields.type };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }
    deletefields(filterId, subCategoryName, opType): Observable<any> {
        let url = this._config.Server + "fields/" + subCategoryName + "?filterId=" + filterId + "&type=" + opType;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }
    editField(val, subCategoryName): Observable<any> {
        let url = this._config.Server + "fields/" + subCategoryName;
        let body = JSON.stringify(val);
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getFilter() {
        let url = this._config.Server + 'filter';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    gotoRowsUpate(currId, otherId, type) {
        let url = this._config.Server + "reorderCategory";
        let body = {
            currId: currId,
            otherId: otherId,
            type: type
        };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

}
