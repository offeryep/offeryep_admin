import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class SubcategoriesService {
    public Url: string;
    constructor(public http: Http, public _config: Configuration) { }

    getSubcategory(categoriesName): Observable<any> {
        let url = this._config.Server + "subCategory?categoryName=" + categoriesName;
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addSubCategory(subCategory, categoriesName): Observable<any> {
        let url = this._config.Server + "subCategory/" + categoriesName;
        let body = JSON.stringify(subCategory);
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    deletesubcategory(subCategoryName, categoriesName, image): Observable<any> {
        let url = this._config.Server + "subCategory/" + categoriesName + "?subCategoryName=" + subCategoryName + "&image=" + image;
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());

    }
    editFields(newSubcat, oldSubcat, catName, otherName, newImage, oldImage): Observable<any> {
        let url = this._config.Server + "subCategory/" + catName;
        let body = {
            newSubCategoryName: newSubcat,
            oldSubCategoryName: oldSubcat,
            otherName: otherName,
            image: newImage,
            oldImage: oldImage
        };
        return this.http.put(url, body, { headers: this._config.headers }).map(res => res.json());
    }

    getLanguage() {
        let url = this._config.Server + 'language';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }

    gotoRowsUpate(currId, otherId, type) {
        let url = this._config.Server + "reorderCategory";
        let body = {
            currId: currId,
            otherId: otherId,
            type: type
        };
        return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
    }

}
