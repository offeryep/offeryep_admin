import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from "../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { PromotionPlanService } from './promotionplans.service';
declare var swal: any;
import { PagesComponent } from '../pages/pages.component';


@Component({
    selector: 'promotionplans',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./promotionplans.component.scss'],
    templateUrl: './promotionplans.component.html',
    providers: [PromotionPlanService]
})

export class PromotionPlansComponent {
    public count: any;
    plans: FormGroup;
    editPlans: FormGroup;
    public p = 1;
    public totalPages: any;
    public rowsOnPage = 10;
    planId: any;
    obj = '';
    promotionData: any = [];
    msg: any = false;
    btnFlag = true;

    constructor(private _appConfig: AppConfig, private _promotionplanservice: PromotionPlanService, private router: Router, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.plans = fb.group({
            'planTitle': ['', Validators.required],
            'inAppPurchaseId': ['', Validators.required],
            'uniqueViews': ['', Validators.required],
            'price': ['', Validators.required],
        });
        this.editPlans = fb.group({
            'planTitle': ['', Validators.required],
            'inAppPurchaseId': ['', Validators.required],
            'uniqueViews': ['', Validators.required],
            'price': ['', Validators.required],
        });
    }

    ngOnInit() {
        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'plans') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","19%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width","19%");  
                        this.btnFlag = false;
                    }
                }
            }
        }

        this._promotionplanservice.getPromotionPlans().subscribe(
            result => {
                if (result.data && result.data.length > 0) {
                    this.promotionData = result.data;
                    // console.log("result", result);
                } else {
                    this.promotionData = [];
                    this.msg = "No Plans Available";
                }

            }
        )
    }
    submitForm(value) {
        // console.log("value", value._value);
        this._promotionplanservice.submitPlan(value._value).subscribe(
            result => {
                // console.log("result", result);
                jQuery('#addPlans').modal('hide');
                this.ngOnInit();
                this.plans.reset();
            }
        )
    }
    gotoEditData(data) {
        this.obj = data;
        // console.log("data",data);
        this.planId = data.planId;
    }
    editPlanData(value) {
        // console.log("data", value._value);
        value._value.planId = this.planId;
        this._promotionplanservice.gotoPlanEditData(value._value).subscribe(
            result => {
                // console.log("result", result);
                jQuery('#editPlans').modal('hide');
                this.ngOnInit();
                this.editPlans.reset();
            }
        )
    }
    gotoDeletePlans(data) {
        var plan = this;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Plan!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    plan.deletePlans(data);
                    swal({
                        title: 'Delete!',
                        text: 'Plan Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Plan is safe :)", "error");
                }
            });
    }
    deletePlans(data) {
        this._promotionplanservice.gotoDeletePlans(data).subscribe(
            result => {
                // console.log("result", result);
                this.ngOnInit();
            }
        )
    }
}