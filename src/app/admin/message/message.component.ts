import { Component, ViewEncapsulation, ViewContainerRef, AfterViewInit, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfig } from "../../app.config";
import { Modal } from 'angular2-modal/plugins/bootstrap';
import { ModalModule } from "ng2-modal";
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl } from '@angular/forms';
import { MessageService } from './message.service';
declare var swal: any;
import { PagesComponent } from '../../pages/pages.component';
declare var sweetAlert: any;


@Component({
    selector: 'language',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./message.component.scss'],
    templateUrl: './message.component.html',
    providers: [MessageService]
})

export class MessageComponent {
    public rowsOnPage = 10;
    msg = false;
    addMessage: FormGroup;
    editMessage: FormGroup;
    isRtl = false;
    data = [];
    messageId: any;
    obj = '';
    btnFlag = true;
    
    constructor(private _service: MessageService, private router: Router, vcRef: ViewContainerRef, fb: FormBuilder, public _isAdmin: PagesComponent) {
        this.addMessage = fb.group({
            'message': ['', Validators.required],
           
        });
        this.editMessage = fb.group({
            'message': ['', Validators.required],
            
        });
    }


    ngOnInit() {

        if (this._isAdmin.isAdmin == false) {
            var role = sessionStorage.getItem('role');
            var roleDt = JSON.parse(role);
            for (var x in roleDt) {
                if (x == 'message') {
                    if (roleDt[x] == 0) {
                        this.router.navigate(['error']);
                    } else if (roleDt[x] == 100) {
                        jQuery(".buttonCus").hide();
                        jQuery(".thAction").remove();
                        jQuery(".thTitle").css("width", "100%");
                        this.btnFlag = false;
                    } else if (roleDt[x] == 110) {
                        this.btnFlag = false;
                        jQuery(".thAction").remove();
                        // jQuery(".thTitle").css("width", "61%");
                    }
                }
            }
        }


        this._service.getMessageData().subscribe(
            res => {
                if (res.data && res.data.length > 0) {
                    this.msg = false;
                    this.data = res.data;
                } else {
                    this.msg = true;
                    this.data = [];
                }
            }
        )
    }

    submitForm(val) {
        this._service.addMessage(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    jQuery('#addLanguage').modal('hide');
                    this.ngOnInit();
                    this.addMessage.reset();
                    swal("Success!", "Message Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", "Something went wrong!", "error");
                }
            }
        )
    }

    gotoEditData(data) {
        this.obj = data.message;
        console.log("edit",data)
        this.messageId = data._id;
    }
    editForm(val) {
        val._value.messageId = this.messageId;
        this._service.gotoEditMessage(val._value).subscribe(
            res => {
                if (res.code == 200) {
                    jQuery('#editLanguage').modal('hide');
                    this.ngOnInit();
                    swal("Success!", "Message Added Successfully!", "success");
                } else {
                    sweetAlert("Oops...", res.message, "error");
                }
            }
        )
    }
    gotoDeleteMessage(msg){
        var cat = this;

        console.log("+++",msg._id)
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this Message!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, I am sure!',
            cancelButtonText: "No, cancel it!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    cat.deleteMessage(msg);
                    swal({
                        title: 'Delete!',
                        text: 'Message Deleted Successfully!',
                        type: 'success'
                    });

                } else {
                    swal("Cancelled", "Your Message is safe :)", "error");
                }
            });
    }
    deleteMessage(msg) {
         console.log("abc", msg)
        this._service.deleteMessage(msg._id).subscribe(
            result => {
                // console.log("result", result);
                // if (result.code != 200) {
                //     sweetAlert("Oops...", "Something went wrong!", "error");
                // }
                // this.dltcategory = result;
                this.ngOnInit();
            }
        )
    }
}