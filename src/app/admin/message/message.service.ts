import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';
import { timeInterval } from 'rxjs/operator/timeInterval';

@Injectable()
export class MessageService {


    constructor(public http: Http, public _config: Configuration) { }

    getMessageData() {
        let url = this._config.Server + 'message';
        return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
    }
    addMessage(val) {
        let url = this._config.Server + 'message';
        return this.http.post(url, JSON.stringify(val), { headers: this._config.headers }).map(res => res.json());
    }
    gotoEditMessage(val) {
        console.log("service",val)
        let url = this._config.Server + 'message';
        var body = {
            messageId:val.messageId,
            message:val.message
        }
        return this.http.put(url,body, { headers: this._config.headers }).map(res => res.json());
    }
    deleteMessage(MessageId) {
        let url = this._config.Server + 'message/'+"?messageId="+MessageId 
        return this.http.delete(url, { headers: this._config.headers }).map(res => res.json());
    }

}