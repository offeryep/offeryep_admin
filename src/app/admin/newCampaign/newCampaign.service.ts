import { Injectable } from '@angular/core';
import { HttpModule, Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Configuration } from '../../app.constant';

@Injectable()
export class NewCampaignService {
  public Url: string;

  constructor(public http: Http, public _config: Configuration) { }

  getTargetUser(offset, limit, lat, long, location, radius, unit) {
    let url = this._config.Server + "campaign/users";
    let body = {
      limit: limit,
      offset: offset,
      location: location,
      latitude: lat,
      longitude: long,
      distanceUnit: unit,
      radius: radius
    };
    return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
  }
  uploadPushImage(image) {
    let url = this._config.Server + "pushImageUpload";
    let body = {
      image: image,
    }
    return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());
  }
  sendCampaign(campTitle, username, image, title, msg, urlLink, cityName, cmpType, start, end) {
    let url = this._config.Server + "sendCampaignMessage";
    let body = {
      campaignTitle: campTitle,
      title: title,
      message: msg,
      pushToken: username,
      url: urlLink,
      imageUrl: image,
      place: cityName,
      type: cmpType,
      startDate: start,
      endDate: end
    }
    return this.http.post(url, body, { headers: this._config.headers }).map(res => res.json());

  }
  getUserDetail(user) {
    let url = this._config.Server + "userDetail?username=" + user;
    return this.http.get(url, { headers: this._config.headers }).map(res => res.json());
  }
}
